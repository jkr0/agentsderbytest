using System.Collections;
using UnityEngine;

public class ExplosionScript : MonoBehaviour
{
    //private AudioSource myAudioSource;           //explosions not working on higher timescales, turned off
    //public AudioClip clip;
    private void Awake()
    {
        //myAudioSource = GetComponent<AudioSource>();
    }
    private void OnEnable()
    {
        //myAudioSource.PlayOneShot(clip);
        StartCoroutine(DestroyMeBaby());
    }
    private IEnumerator DestroyMeBaby()
    {
        yield return new WaitForSeconds(0.2f);
        this.gameObject.SetActive(false);
    }
}
