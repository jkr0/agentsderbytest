using UnityEngine;

public class PotatoScript : MonoBehaviour
{
    private Rigidbody myRigidBody;
    private float speed = 3f;
    private int health = 3;
    private GameObject myHighLight;


    private void Awake()
    {
        myHighLight = transform.GetChild(0).gameObject;
    }
    private void Start()
    {
        myRigidBody = GetComponent<Rigidbody>();
        StartMoving(Vector3.zero);
    }
    public int GetHealth()  {   return health;  }
    public void Reset()
    {
        health = 3;
        speed = 3f;
        myHighLight.SetActive(false);
    }
    public void Damage(Vector3 collisionLocation)
    {
        //Debug.LogError("DMG");
        GameObject explosion = Simulation.Instance.explosionPooler.AskForexplosion();
        //Debug.LogError("DMG1");
        explosion.transform.position = collisionLocation;
        //Debug.LogError("DMG2");
        explosion.SetActive(true);
        //Debug.LogError("DMG3");
        health--;
        if(health <= 0)
        {
            gameObject.SetActive(false);
        }
    }
    private void StartMoving(Vector3 other)
    {
        if (other == Vector3.zero || other == null)
        {
            myRigidBody.velocity = RandomDirection();
        }
        else if(myRigidBody == null)
        {
            return;
        }
        else
        {
            myRigidBody.velocity = NotSoRandomDirection(other);
        }
    }
    Vector3 newDirection = Vector3.one;
    float offset = 0f;
    private Vector3 NotSoRandomDirection(Vector3 avoidPos)              //get direction opposite to contact point
    {
        newDirection = (avoidPos - transform.position);

        offset = Random.Range(-66.69f, 66.69f);             //hehe
        newDirection = Quaternion.Euler(0, offset, 0) * newDirection;
        //Debug.LogError("Blocking Transform: " + avoidPos + "  new direction: " + newDirection + " and my pos: "+transform.position);
        return -newDirection.normalized * speed;
    }
    public void Select()
    {
        myHighLight.SetActive(true);
    }
    public void Deselect()
    {
        myHighLight.SetActive(false);
    }
    private Vector3 RandomDirection()                //go random
    {
        var vector2 = Random.insideUnitCircle.normalized * speed;
        return new Vector3(vector2.x, 0, vector2.y);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<PotatoScript>())
        {
            collision.collider.GetComponent<PotatoScript>().Damage(collision.GetContact(0).point);
            StartMoving(collision.GetContact(0).point);
        }
        else
        {
            StartMoving(Vector3.zero);
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        StartMoving(collision.GetContact(0).point);
    }

}
