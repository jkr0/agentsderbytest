using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{

    public string explosionName = "explosion";
    public List<GameObject> pooledexplosions;
    public GameObject explosionPrefab;
    public int totalexplosions = 6; 
    private void Start()
    {
        pooledexplosions = new List<GameObject>();
        GameObject temp;
        for (int i = 0; i < totalexplosions; i++)
        {
            temp = Instantiate(explosionPrefab);
            temp.SetActive(false);
            temp.name = explosionName + i.ToString();
            pooledexplosions.Add(temp);
            temp.transform.SetParent(this.transform);
        }
    }
    public int GetNumberOfActiveexplosions()
    {
        int currentNumber = 0;
        for (int i = 0; i < totalexplosions; i++)
        {
            if (pooledexplosions[i].activeInHierarchy) currentNumber++;
        }
        return currentNumber;
    }
    public GameObject AskForexplosion()
    {
        for (int i = 0; i < totalexplosions; i++)
        {
            if (!pooledexplosions[i].activeInHierarchy)
            {
                return pooledexplosions[i];
            }
        }
        totalexplosions++;                // make it flexible
        GameObject temp = Instantiate(explosionPrefab);
        temp.SetActive(false);
        pooledexplosions.Add(temp);
        temp.name = explosionName + (totalexplosions - 1).ToString();
        temp.transform.SetParent(this.transform);
        return pooledexplosions[totalexplosions - 1];
    }
}
