using UnityEngine;

public class CameraController : MonoBehaviour
{
    private float cameraSpeed = 30f;
    //private float rotationSpeed = 50f;
    private float zoomSpeed = 150f;
    private float maxHangedDistance = 50f;
    private float minHangedDistance = 10f;

    //public enum CameraMode
    //{
        //hanged
        //, free
        //,fps
    //}
    private Vector3 tempVector;
    private void ShouldMoveAround()
    {
        //switch (myMode)
        //{
        //    case CameraMode.free:
        //        {
        //            if (Input.GetKey(KeyCode.W))
        //            {
        //                transform.position = transform.position + (transform.forward * cameraSpeed * Time.deltaTime);
        //            }
        //            if (Input.GetKey(KeyCode.A))
        //            {
        //                transform.position = transform.position + (-transform.right * cameraSpeed * Time.deltaTime);
        //            }
        //            if (Input.GetKey(KeyCode.S))
        //            {
        //                transform.position = transform.position + (-transform.forward * cameraSpeed * Time.deltaTime);
        //            }
        //            if (Input.GetKey(KeyCode.D))//right
        //            {
        //                transform.position = transform.position + (transform.right * cameraSpeed * Time.deltaTime);
        //            }
        //            if (Input.GetKey(KeyCode.Q))//down
        //            {
        //                transform.position = transform.position + (Vector3.down * cameraSpeed * Time.deltaTime);
        //            }
        //            if (Input.GetKey(KeyCode.E))//up
        //            {
        //                transform.position = transform.position + (Vector3.up * cameraSpeed * Time.deltaTime);
        //            }

        //            //mouse movement to camera movement
        //            transform.rotation *= Quaternion.AngleAxis(Time.deltaTime * 5 * Input.GetAxis("Mouse X"), Vector3.up);

        //            transform.rotation *= Quaternion.AngleAxis(Time.deltaTime * 5 * Input.GetAxis("Mouse Y"), Vector3.left);

        //            break;
        //        }
        //    case CameraMode.hanged:
        //        {
        if (Input.GetKey(KeyCode.W))//forward
        {
            transform.position = transform.position + (Vector3.forward * cameraSpeed * Time.unscaledDeltaTime);
        }
        if (Input.GetKey(KeyCode.S))//back
        {
            transform.position = transform.position + (Vector3.back * cameraSpeed * Time.unscaledDeltaTime);
        }
        if (Input.GetKey(KeyCode.A))//left
        {
            transform.position = transform.position + (Vector3.left * cameraSpeed * Time.unscaledDeltaTime);
        }
        if (Input.GetKey(KeyCode.D))//right
        {
            transform.position = transform.position + (Vector3.right * cameraSpeed * Time.unscaledDeltaTime);
        }
        if (Input.GetKey(KeyCode.Q))//down
        {
            tempVector = transform.position;
            tempVector = transform.position + (Vector3.down * cameraSpeed * Time.unscaledDeltaTime);
            tempVector.y = Mathf.Clamp(tempVector.y, minHangedDistance, maxHangedDistance);
            transform.position = tempVector;
        }
        if (Input.GetKey(KeyCode.E))//up
        {

            tempVector = transform.position;
            tempVector = transform.position + (Vector3.up * cameraSpeed * Time.unscaledDeltaTime);
            tempVector.y = Mathf.Clamp(tempVector.y, minHangedDistance, maxHangedDistance);
            transform.position = tempVector;
        }
        if (Input.mouseScrollDelta != Vector2.zero)//przyblizanie oddalanie
        {
            tempVector = transform.position;
            tempVector = transform.position + (transform.forward * zoomSpeed * Time.unscaledDeltaTime * (Input.mouseScrollDelta.y > 0 ? 1 : -1));
            tempVector.y = Mathf.Clamp(tempVector.y, minHangedDistance, maxHangedDistance);
            transform.position = tempVector;
        }

        //    break;
        //}
        //}
    }
    //public CameraMode myMode = CameraMode.hanged;
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.C))
        //{
        //    int temp = (int)myMode;
        //    temp++;
        //    if (temp > 1) temp = 0;
        //    myMode = (CameraMode)temp;
        //    if (myMode == CameraMode.hanged)
        //    {
        //        transform.position = new Vector3(0, 50f, -21f);
        //        transform.rotation.SetEulerRotation(69.6f, 0, 0); //= new Vector3(69.6f, 0, 0);
        //    }
        //}
        ShouldMoveAround();
    }

}
