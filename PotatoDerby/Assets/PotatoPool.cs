using System.Collections.Generic;
using UnityEngine;

public class PotatoPool : MonoBehaviour
{    
    public string potatoName = "potato";
    public List<GameObject> pooledPotatos;
    public GameObject potatoPrefab;
    public int totalPotatoes = 30;               //can exceed, just default value to not make memory go brrr
    
    private void Start()
    {

        pooledPotatos = new List<GameObject>();
        GameObject temp;
        for (int i = 0; i < totalPotatoes; i++)
        {
            temp = Instantiate(potatoPrefab);
            temp.SetActive(false);
            temp.name = potatoName + i.ToString();
            temp.transform.SetParent(this.transform);
            pooledPotatos.Add(temp);
        }
    }
    public int GetNumberOfActivePotatoes()
    {
        int currentNumber = 0;
        for (int i = 0; i < totalPotatoes; i++)
        {
            if (pooledPotatos[i].activeInHierarchy) currentNumber++;
        }
        return currentNumber;
    }
    public GameObject AskForPotato()
    {
        for (int i = 0; i < totalPotatoes; i++)
        {
            if (!pooledPotatos[i].activeInHierarchy)
            {
                return pooledPotatos[i];
            }
        }
        totalPotatoes++;                // make it flexible
        GameObject temp = Instantiate(potatoPrefab);
        temp.SetActive(false);
        pooledPotatos.Add(temp);
        temp.name = potatoName + (totalPotatoes-1).ToString();
        temp.transform.SetParent(this.transform);
        return pooledPotatos[totalPotatoes - 1];
    }
}
