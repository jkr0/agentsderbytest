using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    PotatoScript currentPotato;
    private Camera _cam;
    private Camera myCamera
    {
        get
        {
            if(_cam == null)
            {
                _cam = Camera.main;
                
            }
            return _cam;
        }
    }
    private void SelectPotato(PotatoScript potato)
    {
        UIManager.Instance.ShowPotatoInfo(potato);
        if (currentPotato != null)
        currentPotato.Deselect();
        if (potato != null)
        {
            potato.Select();
            currentPotato = potato;

        }
    }
    private RaycastHit hitData;
    private void Update()
    {
        if(currentPotato != null && currentPotato.GetHealth() <= 0)
        {
            SelectPotato(null);
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hitData, 1000000, 1, QueryTriggerInteraction.Collide))
            {
                PotatoScript hitPotato = hitData.transform.gameObject.GetComponent<PotatoScript>();
                SelectPotato(hitPotato);
            }
        }
    }
}
