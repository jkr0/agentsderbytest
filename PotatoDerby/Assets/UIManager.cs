using UnityEngine;
using TMPro;
using System.Text;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    [SerializeField]
    private GameObject statsObject;
    [SerializeField]
    private TextMeshProUGUI potatoName, potatoHealth, MarcoPoloText;
    private PotatoScript currentPotato;
    public GameObject marcoPoloMainWindow;
    public UnityEngine.UI.ScrollRect MarcoPoloScrollRect;
    private void Start()
    {
        Instance = this;
    }
    public void ShowPotatoInfo(PotatoScript potato)
    {
        if(potato != null)
        {
            statsObject.SetActive(true);
            potatoName.text = potato.gameObject.name;
            currentPotato = potato;
        }
        else
        {
            statsObject.SetActive(false);
            currentPotato = potato;
        }
    }
    private void LateUpdate()
    {
        if (currentPotato != null)
        {
            potatoHealth.text = currentPotato.GetHealth().ToString();
        }
    }
    public void CloseMarcoPolo()
    {
        marcoPoloMainWindow.SetActive(false);
    }
    public void MarcoPolo()
    {
        
        StringBuilder text = new StringBuilder();
        for (int i = 1; i <= 100; i++)
        {
            text.Append(i.ToString());
            if(i%3 == 0)
            {
                text.Append(" Marco");
            }
            if(i%5 == 0)
            {
                text.Append(" Polo");
            }
            text.Append("\n");
        }
        MarcoPoloText.text = text.ToString();
        marcoPoloMainWindow.SetActive(true);
    }
}
