using UnityEngine;

public class Simulation : MonoBehaviour
{
    [Header("Setup")]
    [SerializeField , Tooltip("If off, will spawn potatoes randomly from 2 - 10 seconds")]
    private bool useInspectorRespawnTime;
    [SerializeField,Range(2f, 10f)]
    private float customRespawnTime;
    //[SerializeField]
    private float currentTime = 0f;

    [SerializeField]
    private float minRespawnTime = 2f;
    [SerializeField]
    private float maxRespawnTime = 10f;
    [SerializeField]
    private Transform spawnPoint;
    [SerializeField]
    private int maxPotatoesNumber;


    private PotatoPool pooler;


    public static Simulation Instance;
    public ObjectPooler explosionPooler;


    private void Awake()
    {
        Instance = this;
        pooler = GetComponent<PotatoPool>();
        explosionPooler = GetComponentInChildren<ObjectPooler>();
    }
    private void SpawnPotato()
    {
        if(pooler.GetNumberOfActivePotatoes() >= maxPotatoesNumber)
        {
            currentTime = 0.25f; //amount of time to wait before checking again
        }
        currentTime = useInspectorRespawnTime ? customRespawnTime : Random.Range(minRespawnTime, maxRespawnTime);
        GameObject potato = pooler.AskForPotato();
        potato.transform.position = spawnPoint.position;
        potato.GetComponent<PotatoScript>().Reset();
        potato.SetActive(true);
    }


    public void FixedUpdate()
    {
        currentTime -= Time.deltaTime;
        if(currentTime < 0)
        {
            SpawnPotato();
        }
    }
}
