using UnityEngine;
using UnityEngine.UI;

public class ControlTime : MonoBehaviour
{
    Slider mySlider;
    private void Awake()
    {
        mySlider = GetComponent<Slider>();
        
    }
    public void ResetSlider()
    {
        mySlider.value = 1f;
    }
    private void Update()
    {
        Time.timeScale = mySlider.value;
    }
}
