
using UnityEngine;

public class RotateRight : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, -Time.deltaTime * 100);
    }
}
